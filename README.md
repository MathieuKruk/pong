# Pong Game

## ◾ Features

<p align="center">
  <img src="https://static01.nyt.com/images/2018/06/01/obituaries/31DABNEY1/31DABNEY1-superJumbo.jpg?quality=90&auto=webp">
</p>



## ◾ Motivation

It has been launch the 💠16/03/2020💠 



## ◾ Tasks

- [x] Initialize the ReadMe.
- [x] Work on the main project.
- [ ] Launch the app.




## ◾ Build-status

📲 V1.0.1
Logs

   - Readme created.
   - .gitignore created.
   - Basic 2 players Pong game.
   - Basic design.
   - Actions: Reset.
   - Informative box added. 
   - Migrated to Pug.




##  ◾ Screenshots


![screenshot](./screenshot.png)



### ◾ Technology

- Front-end
   - [PUG](https://pugjs.org/api/getting-started.html)

   - [SCSS](https://sass-lang.com/)

     

- Back-end

  - [Express.js](https://expressjs.com/fr/)

  

## ◾ Code-Example

### PUG code: 

```PUG
.grid 
            .header
                h1 XSI PONG GAME

            #board
                .bar#player-one-bar
                .bar#player-two-bar
                #ball

            .box#buttons
                h3 Buttons:
                button(type="reset")#restart-btn Restart

            .box#infos
                h3 Controls:
                ul
                    li
                        p Player One (bottom) controls
                        ul
                            li Right: right arrow
                            li Left: left arrow
                    li
                        p Player Two (top) controls
                        ul
                            li Right: S
                            li  Left: A
            
            .box#score
                h3 Score:
```



## ◾ Credits

   - 👨‍💻  [Mathieu Kruk](https://github.com/MathieuKruk)



## ◾ License

OpenSource | Free

<p align="right">
  <img src="https://i.imgur.com/y61Nh57.gif" height="40%" width="25%">
</p>


